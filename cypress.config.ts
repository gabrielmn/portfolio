import { defineConfig } from 'cypress';
import { rmSync, existsSync } from 'fs';

export default defineConfig({
    e2e: {
        setupNodeEvents(on, config) {
            // implement node event listeners here
            on('task', {
                rm(path: string) {
                    if (existsSync(path)) {
                        rmSync(path);
                        return true;
                    }
                    return false;
                }
            });
        },
        specPattern: 'src/**/*.cy.{js,jsx,ts,tsx}',
        screenshotOnRunFailure: false,
        video: false
    }
});
