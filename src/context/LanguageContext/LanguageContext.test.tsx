import React, { Fragment } from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';

import { LanguageProvider, LanguageConsumer } from './LanguageContext';


describe('Supported languages', () => {

    it('en-us', () => {
        Object.defineProperty(navigator, 'language', {
            value: 'en-us',
            configurable: true
        });
        render(
            <LanguageProvider>
                <LanguageConsumer>
                    {
                        (context) => {
                            expect(context.language).toBe('en-us');
                            return(<Fragment></Fragment>);
                        }
                    }
                </LanguageConsumer>
            </LanguageProvider>
        );
    });

    it('pt-br', () => {
        Object.defineProperty(navigator, 'language', {
            value: 'pt-br',
            configurable: true
        });
        render(
            <LanguageProvider>
                <LanguageConsumer>
                    {
                        (context) => {
                            expect(context.language).toBe('pt-br');
                            return(<Fragment></Fragment>);
                        }
                    }
                </LanguageConsumer>
            </LanguageProvider>
        );
    });
});

describe('Not supported languages', () => {

    it('en', () => {
        Object.defineProperty(navigator, 'language', {
            value: 'en',
            configurable: true
        });
        render(
            <LanguageProvider>
                <LanguageConsumer>
                    {
                        (context) => {
                            expect(context.language).toBe('en-us');
                            return(<Fragment></Fragment>);
                        }
                    }
                </LanguageConsumer>
            </LanguageProvider>
        );
    });
});

describe('[WCAG] Accessibility test', () => {

    it('[3.1.1] en-us', () => {
        Object.defineProperty(navigator, 'language', {
            value: 'en-us',
            configurable: true
        });
        render(
            <LanguageProvider>
            </LanguageProvider>
        );

        expect(document.documentElement.lang).toBe('en-us');
    });

    it('[3.1.1] pt-br', () => {
        Object.defineProperty(navigator, 'language', {
            value: 'pt-br',
            configurable: true
        });
        render(
            <LanguageProvider>
            </LanguageProvider>
        );

        expect(document.documentElement.lang).toBe('pt-br');
    });
});