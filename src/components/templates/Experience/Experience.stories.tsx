import React from 'react';
import { ComponentMeta, Story } from '@storybook/react';
import logo from '../../../assets/images/logo.png';

import { Experience, ExperienceType } from './Experience';

export default {
    title: 'templates/Experience',
    component: Experience,
    argTypes:{
        source:{
            control: {
                type: 'file',
                accept: '.png'
            }
        },
        className:{
            control: false
        }
    },
    parameters: {
        layout: 'centered'
    },
} as ComponentMeta<typeof Experience>;

const experience:ExperienceType = {
    image: logo,
    company: 'Test Inc.',
    position: 'Fullstack',
    startDate: '01/2023',
    endDate: 'Ongoing',
    responsibilities: [
        {id: '1', responsibility: 'Test legacy code'},
        {id: '2', responsibility: 'Refactor legacy code'}
    ],
    id: '',
    type: 'Intership',
};

const template: Story = (args) => <Experience experience={args.experience}/>;

export const basic = template.bind({});
basic.args = {
    id: 'storybook-image',
    source: logo,
    pastExperience: experience
};