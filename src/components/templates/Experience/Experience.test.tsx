import React from 'react';
import { render, screen } from '@testing-library/react';
import { LanguageProvider } from '../../../context/LanguageContext/LanguageContext';
import { Experience, ExperienceType } from './Experience';

import { axe, toHaveNoViolations } from 'jest-axe';
expect.extend(toHaveNoViolations);

import strings from './strings.json';

const experience: ExperienceType = {
    image: '',
    company: 'Test Inc.',
    position: 'Fullstack',
    startDate: '01/2023',
    endDate: 'Ongoing',
    responsibilities: [
        {id: '1', responsibility: 'Test legacy code'},
        {id: '2', responsibility: 'Refactor legacy code'}
    ],
    id: '',
    type: 'Intership'
};

describe('Experience component test', () => {

    const language = 'en-us';

    test('render', () => {
        render(
            <LanguageProvider>
                <Experience experience={experience} />
            </LanguageProvider>
        );
        const image = screen.getByAltText(`${strings[language]['company']} ${experience.company} logo`);
        expect(image).toBeInTheDocument();
        const company = screen.getByText(experience.company);
        expect(company).toBeInTheDocument();
        const position = screen.getByText(experience.position, {exact: false});
        expect(position).toBeInTheDocument();
        const type = screen.getByText(experience.type, {exact: false});
        expect(type).toBeInTheDocument();
        const startDate = screen.getByText(experience.startDate, {exact: false});
        expect(startDate).toBeInTheDocument();
        const endDate = screen.getByText(experience.endDate, {exact: false});
        expect(endDate).toBeInTheDocument();
        experience.responsibilities.forEach((item)=>{
            const responsibility = screen.getByText(item.responsibility);
            expect(responsibility).toBeInTheDocument();
        });
    });

    test('accessibility', async() => {
        const { container } = render(
            <LanguageProvider>
                <Experience experience={experience} />
            </LanguageProvider>
        );
        const image = screen.getByAltText(`${strings[language]['company']} ${experience.company} logo`);
        expect(image).toBeInTheDocument();
        const company = screen.getByText(experience.company);
        expect(company).toBeInTheDocument();
        const position = screen.getByText(experience.position, {exact: false});
        expect(position).toBeInTheDocument();
        const type = screen.getByText(experience.type, {exact: false});
        expect(type).toBeInTheDocument();
        const startDate = screen.getByText(experience.startDate, {exact: false});
        expect(startDate).toBeInTheDocument();
        const endDate = screen.getByText(experience.endDate, {exact: false});
        expect(endDate).toBeInTheDocument();
        experience.responsibilities.forEach((item)=>{
            const responsibility = screen.getByText(item.responsibility);
            expect(responsibility).toBeInTheDocument();
        });
        
        // disable due to testing without a list container(ul)
        const results = await axe(container, {
            rules: {
                'listitem': { enabled: false }
            }
        });
        expect(results).toHaveNoViolations();
    });
});

describe('[Props] Experience component test', () => {

    const language = 'en-us';

    test('pastExperience', () => {
        render(
            <LanguageProvider>
                <Experience  experience={experience} />
            </LanguageProvider>
        );
        const image = screen.getByAltText(`${strings[language]['company']} ${experience.company} logo`);
        expect(image).toBeInTheDocument();
        const company = screen.getByText(experience.company);
        expect(company).toBeInTheDocument();
        const position = screen.getByText(experience.position, {exact: false});
        expect(position).toBeInTheDocument();
        const type = screen.getByText(experience.type, {exact: false});
        expect(type).toBeInTheDocument();
        const startDate = screen.getByText(experience.startDate, {exact: false});
        expect(startDate).toBeInTheDocument();
        const endDate = screen.getByText(experience.endDate, {exact: false});
        expect(endDate).toBeInTheDocument();
        experience.responsibilities.forEach((item)=>{
            const responsibility = screen.getByText(item.responsibility);
            expect(responsibility).toBeInTheDocument();
        });
    });

});