import React from 'react';
import { ComponentMeta, Story } from '@storybook/react';
import logo from '../../../assets/images/logo.png';

import { Project, ProjectType } from './Project';

export default {
    title: 'templates/Project',
    component: Project,
    argTypes:{
        image:{
            control: {
                type: 'file',
                accept: '.png'
            }
        },
    },
    parameters: {
        layout: 'centered'
    },
} as ComponentMeta<typeof Project>;

const project:ProjectType ={
    name: 'Storybook',
    image: logo,
    topics: [
        { id: '1', topic: 'C' },
        { id: '2', topic: 'C++' }
    ],
    description: 'description',
    images: [logo, logo],
    id: ''
};

const template: Story = (args) => <Project project={project}/>;

export const basic = template.bind({});