import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { LanguageProvider } from '../../../context/LanguageContext/LanguageContext';
import { Project, ProjectType } from './Project';
import logo from '../../../assets/images/logo.png';

import { axe, toHaveNoViolations } from 'jest-axe';
expect.extend(toHaveNoViolations);

import strings from './strings.json';

const project: ProjectType = {
    name: 'Project Test',
    image: logo,
    topics: [
        { id: '1', topic: 'C' },
        { id: '2', topic: 'C++' }
    ],
    description: 'description',
    images: [logo, logo],
    id: ''
};

describe('Project component test', () => {

    const language = 'en-us';

    test('render', () => {
        render(
            <LanguageProvider>
                <Project project={project} />
            </LanguageProvider>
        );
        const image = screen.getByAltText(`${strings[language]['project']} ${project.name} ${strings[language]['image']}`);
        expect(image).toBeInTheDocument();
        const name = screen.getByText(project.name);
        expect(name).toBeInTheDocument();
        project.topics.forEach((item) => {
            const tag = screen.getByText(item.topic);
            expect(tag).toBeInTheDocument();
        });
    });

    test('accessibility', async () => {
        const { container } = render(
            <LanguageProvider>
                <Project project={project} />
            </LanguageProvider>
        );
        const image = screen.getByAltText(`${strings[language]['project']} ${project.name} ${strings[language]['image']}`);
        expect(image).toBeInTheDocument();
        const name = screen.getByText(project.name);
        expect(name).toBeInTheDocument();
        project.topics.forEach((item) => {
            const tag = screen.getByText(item.topic);
            expect(tag).toBeInTheDocument();
        });

        // disable due to testing without a list container(ul)
        const results = await axe(container, {
            rules: {
                'listitem': { enabled: false }
            }
        });
        expect(results).toHaveNoViolations();
    });

});

describe('[Props] Project component test', () => {

    const language = 'en-us';

    test('render', () => {
        render(
            <LanguageProvider>
                <Project project={project} />
            </LanguageProvider>
        );
        const image = screen.getByAltText(`${strings[language]['project']} ${project.name} ${strings[language]['image']}`);
        expect(image).toBeInTheDocument();
        const name = screen.getByText(project.name);
        expect(name).toBeInTheDocument();
        project.topics.forEach((item) => {
            const tag = screen.getByText(item.topic);
            expect(tag).toBeInTheDocument();
        });
    });

});