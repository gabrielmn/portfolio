import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Certificate, CertificateType } from './Certificate';

export default {
    title: 'templates/Certificate',
    component: Certificate,
    parameters: {
        layout: 'centered'
    }
} as ComponentMeta<typeof Certificate>;

const certificate: CertificateType = {
    name: 'Testing',
    conclusionDate: '02/2023',
    topics: [
        { id: '1', topic: 'Unit testing' },
        { id: '2', topic: 'Integration testing' },
        { id: '3', topic: 'End to end testing' },
    ],
    link: 'localhost:3000',
    id: '1'
};

const Template: ComponentStory<typeof Certificate> = (args) => <Certificate certificate={certificate}/>;

export const Primary = Template.bind({});
