import React from 'react';
import { render, screen } from '@testing-library/react';
// import and setup axe
import { axe, toHaveNoViolations } from 'jest-axe';
expect.extend(toHaveNoViolations);
// import component
import { Certificate, CertificateType } from './Certificate';

const certificate: CertificateType = {
    name: 'Testing',
    conclusionDate: '02/2023',
    topics: [
        { id: '1', topic: 'Unit testing' },
        { id: '2', topic: 'Integration testing' },
        { id: '3', topic: 'End to end testing' },
    ],
    link: 'localhost:3000',
    id: '1'
};


describe('Certificate component test', () => {

    test('render', () => {
        render(<Certificate certificate={certificate} />);
        const title = screen.getByText(certificate.name);
        expect(title).toBeInTheDocument();
        const date = screen.getByText(certificate.conclusionDate);
        expect(date).toBeInTheDocument();
        certificate.topics.forEach((item) => {
            const description = screen.getByText(item.topic);
            expect(description).toBeInTheDocument();
        });
        const link = screen.getByText(certificate.link);
        expect(link).toBeInTheDocument();
    });

    test('accessibility', async () => {
        const { container } = render(<Certificate certificate={certificate} />);
        const title = screen.getByText(certificate.name);
        expect(title).toBeInTheDocument();
        const date = screen.getByText(certificate.conclusionDate);
        expect(date).toBeInTheDocument();
        certificate.topics.forEach((item) => {
            const description = screen.getByText(item.topic);
            expect(description).toBeInTheDocument();
        });
        const link = screen.getByText(certificate.link);
        expect(link).toBeInTheDocument();

        // disable due to testing without a list container(ul)
        const results = await axe(container, {
            rules: {
                'listitem': { enabled: false }
            }
        });
        expect(results).toHaveNoViolations();
    });
});

describe('[Props] Certificate component test', () => {

    test('title', () => {
        render(<Certificate certificate={certificate} />);
        const title = screen.getByText(certificate.name);
        expect(title).toBeInTheDocument();
    });

    test('date', () => {
        render(<Certificate certificate={certificate} />);
        const date = screen.getByText(certificate.conclusionDate);
        expect(date).toBeInTheDocument();
    });

    test('description', () => {
        render(<Certificate certificate={certificate} />);
        certificate.topics.forEach((item) => {
            const description = screen.getByText(item.topic);
            expect(description).toBeInTheDocument();
        });
    });

    test('link', () => {
        render(<Certificate certificate={certificate} />);
        const link = screen.getByText(certificate.link);
        expect(link).toBeInTheDocument();
    });

});