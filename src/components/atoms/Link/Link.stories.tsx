import React from 'react';
import { ComponentMeta, Story } from '@storybook/react';
import { Link } from './Link';

export default {
    title: 'atoms/Link',
    component: Link,
    argTypes: {
        className:{
            control: false
        }
    },
    parameters: {
        layout: 'centered'
    }
} as ComponentMeta<typeof Link>;

const template: Story = (args) => {

    return (<Link href={args.href}>link</Link>);

};

export const basic = template.bind({});