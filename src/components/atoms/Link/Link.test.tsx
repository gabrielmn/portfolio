import React from 'react';
import { render, screen } from '@testing-library/react';
import { Link } from './Link';

import { axe, toHaveNoViolations } from 'jest-axe';
expect.extend(toHaveNoViolations);


describe('Link component test', () => {

    it('render', async () => {
        render(
            <Link
                href={'localhost'}>
                link
            </Link>);
        const elementLink = screen.getByText('link');
        expect(elementLink).toBeInTheDocument();
    });

    it('accessibility', async () => {
        const { container } = render(
            <Link
                href={'localhost'}>
                link
            </Link>);
        const elementLink = screen.getByText('link');
        expect(elementLink).toBeInTheDocument();
        const results = await axe(container);
        expect(results).toHaveNoViolations();
    });
});

describe('[Props] Button component test', () => {

    it('href', () => {
        render(
            <Link
                href={'localhost'}>
                link
            </Link>);
        const elementLink = screen.getByText('link');
        expect(elementLink).toHaveAttribute('href', 'localhost');
    });

    it('children', () => {
        render(
            <Link
                href={'localhost'}>
                link
            </Link>);
        const elementLink = screen.getByText('link');
        expect(elementLink).toBeInTheDocument();
    });

    it('className', () => {
        render(
            <Link
                className='class'
                href={'localhost'}>
                link
            </Link>);
        const elementLink = screen.getByText('link');
        expect(elementLink.className.includes('class')).toBe(true);
    });
});