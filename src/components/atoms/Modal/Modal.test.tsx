import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { Modal } from './Modal';

import { axe, toHaveNoViolations } from 'jest-axe';
expect.extend(toHaveNoViolations);

class State {

    show: boolean;

    constructor(value: boolean) {
        this.show = value;
    }

    hide() {
        this.show = false;
    }

}

describe('Modal component test', () => {

    it('render', async () => {

        const state = new State(true);

        render(
            <Modal show={state.show} hide={state.hide}>
                hey
            </Modal>
        );

        const dummyText = screen.getByText('hey');
        expect(dummyText).toBeInTheDocument();
    });

});

describe('[Props] Modal component test', () => {

    it('show', async () => {

        const state = new State(true);


        const { container } = render(
            <Modal show={state.show} hide={state.hide}>
                hey
            </Modal>
        );

        const dummyText = screen.getByText('hey');
        expect(dummyText).toBeInTheDocument();
        state.hide();

        render(
            <Modal show={state.show} hide={state.hide}>
                hey
            </Modal>
            , { container }
        );

        expect(dummyText).not.toBeInTheDocument();
    });

});