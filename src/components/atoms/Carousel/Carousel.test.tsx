import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { Carousel, CarouselItem } from './Carousel';
import { LanguageProvider } from '../../../context/LanguageContext/LanguageContext';

import { axe, toHaveNoViolations } from 'jest-axe';
expect.extend(toHaveNoViolations);

import strings from './strings.json';

describe('Carousel component test', () => {

    const language = 'en-us';

    it('render', async () => {
        render(
            <LanguageProvider>
                <Carousel>
                    <CarouselItem>
                        1
                    </CarouselItem>
                    <CarouselItem>
                        2
                    </CarouselItem>
                </Carousel>
            </LanguageProvider>
        );
        // controls
        const previousButton = screen.getByTitle(strings[language]['button-previous']);
        expect(previousButton).toBeInTheDocument();
        const nextButton = screen.getByTitle(strings[language]['button-next']);
        expect(nextButton).toBeInTheDocument();
        // slider
        const firstIndex = screen.getByTitle(`${strings[language]['item']} 1`);
        expect(firstIndex).toBeInTheDocument();
        const secondIndex = screen.getByTitle(`${strings[language]['item']} 2`);
        expect(secondIndex).toBeInTheDocument();
        // items
        const firstItem = screen.getByText('1');
        expect(firstItem).toBeInTheDocument();
    });

    it('accessibility', async () => {
        const { container } = render(
            <LanguageProvider>
                <Carousel>
                    <CarouselItem>
                        1
                    </CarouselItem>
                    <CarouselItem>
                        2
                    </CarouselItem>
                </Carousel>
            </LanguageProvider>
        );
        // controls
        const previousButton = screen.getByTitle(strings[language]['button-previous']);
        expect(previousButton).toBeInTheDocument();
        const nextButton = screen.getByTitle(strings[language]['button-next']);
        expect(nextButton).toBeInTheDocument();
        // slider
        const firstIndex = screen.getByTitle(`${strings[language]['item']} 1`);
        expect(firstIndex).toBeInTheDocument();
        const secondIndex = screen.getByTitle(`${strings[language]['item']} 2`);
        expect(secondIndex).toBeInTheDocument();
        // items
        const firstItem = screen.getByText('1');
        expect(firstItem).toBeInTheDocument();

        const results = await axe(container, {
            rules:{
                'list': { enabled: false }
            }
        });
        expect(results).toHaveNoViolations();
    });

});



describe('[Navigation] Carousel component test', () => {

    const language = 'en-us';

    it('next button', () => {
        render(
            <LanguageProvider>
                <Carousel>
                    <CarouselItem>
                        1
                    </CarouselItem>
                    <CarouselItem>
                        2
                    </CarouselItem>
                </Carousel>
            </LanguageProvider>
        );
        // controls
        const previousButton = screen.getByTitle(strings[language]['button-previous']);
        expect(previousButton).toBeInTheDocument();
        const nextButton = screen.getByTitle(strings[language]['button-next']);
        expect(nextButton).toBeInTheDocument();
        // items
        const item = screen.getByText('1');
        expect(item).toBeInTheDocument();

        fireEvent.click(nextButton);
        expect(item).toHaveTextContent('2');

    });

    it('next button overflow', () => {
        render(
            <LanguageProvider>
                <Carousel>
                    <CarouselItem>
                        1
                    </CarouselItem>
                    <CarouselItem>
                        2
                    </CarouselItem>
                </Carousel>
            </LanguageProvider>
        );
        // controls
        const previousButton = screen.getByTitle(strings[language]['button-previous']);
        expect(previousButton).toBeInTheDocument();
        const nextButton = screen.getByTitle(strings[language]['button-next']);
        expect(nextButton).toBeInTheDocument();
        // items
        const item = screen.getByText('1');
        expect(item).toBeInTheDocument();

        fireEvent.click(nextButton);
        expect(item).toHaveTextContent('2');

        fireEvent.click(nextButton);
        expect(item).toHaveTextContent('1');
    });

    it('previous button', () => {
        render(
            <LanguageProvider>
                <Carousel>
                    <CarouselItem>
                        1
                    </CarouselItem>
                    <CarouselItem>
                        2
                    </CarouselItem>
                </Carousel>
            </LanguageProvider>
        );
        // controls
        const previousButton = screen.getByTitle(strings[language]['button-previous']);
        expect(previousButton).toBeInTheDocument();
        const nextButton = screen.getByTitle(strings[language]['button-next']);
        expect(nextButton).toBeInTheDocument();
        // items
        const item = screen.getByText('1');
        expect(item).toBeInTheDocument();

        fireEvent.click(nextButton);
        expect(item).toHaveTextContent('2');

        fireEvent.click(previousButton);
        expect(item).toHaveTextContent('1');
    });

    it('previous button underflow', () => {
        render(
            <LanguageProvider>
                <Carousel>
                    <CarouselItem>
                        1
                    </CarouselItem>
                    <CarouselItem>
                        2
                    </CarouselItem>
                </Carousel>
            </LanguageProvider>
        );
        // controls
        const previousButton = screen.getByTitle(strings[language]['button-previous']);
        expect(previousButton).toBeInTheDocument();
        const nextButton = screen.getByTitle(strings[language]['button-next']);
        expect(nextButton).toBeInTheDocument();
        // items
        const item = screen.getByText('1');
        expect(item).toBeInTheDocument();

        fireEvent.click(previousButton);
        expect(item).toHaveTextContent('2');

    });

    it('slider', () => {
        render(
            <LanguageProvider>
                <Carousel>
                    <CarouselItem>
                        1
                    </CarouselItem>
                    <CarouselItem>
                        2
                    </CarouselItem>
                </Carousel>
            </LanguageProvider>
        );
        // controls
        // slider
        const firstIndex = screen.getByTitle(`${strings[language]['item']} 1`);
        expect(firstIndex).toBeInTheDocument();
        const secondIndex = screen.getByTitle(`${strings[language]['item']} 2`);
        expect(secondIndex).toBeInTheDocument();
        // items
        const item = screen.getByText('1');
        expect(item).toBeInTheDocument();

        fireEvent.click(secondIndex);
        expect(item).toHaveTextContent('2');
        
    });

});