import React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import { Carousel, CarouselItem } from './Carousel';

export default {
    title: 'atoms/Carousel',
    component: Carousel,
    argTypes: {
        children: {
            control: 'text',
            defaultValue: 'Button'
        },
        className: {
            control: 'text'
        }
    },
    parameters: {
        layout: 'centered'
    }
} as ComponentMeta<typeof Carousel>;

const template: ComponentStory<typeof Carousel> = (args) => (
    <Carousel>
        <CarouselItem>
            {args.children}
        </CarouselItem>
        <CarouselItem>
            {args.children}
        </CarouselItem>
        <CarouselItem>
            {args.children}
        </CarouselItem>
        <CarouselItem>
            {args.children}
        </CarouselItem>
        <CarouselItem>
            {args.children}
        </CarouselItem>
    </Carousel>
);

export const basic = template.bind({});
