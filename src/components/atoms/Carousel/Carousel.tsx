import React, { Component, ReactNode, ReactElement, MouseEvent, ContextType } from 'react';

import { ReactComponent as BeforeIcon } from '../../../assets/icons/navigate_before_48dp.svg';
import { ReactComponent as NextIcon } from '../../../assets/icons/navigate_next_48dp.svg';

// context
import { LanguageContext } from '../../../context/LanguageContext/LanguageContext';

import classes from './Carousel.module.css';
import strings from './strings.json';


export interface CarouselProps {
    children: ReactElement<CarouselItem>[];
    className?: string;

    indicatorPosition?: 'top' | 'bottom';
}

export interface CarouselState {
    index: number;
}

export class Carousel extends Component<CarouselProps, CarouselState> {

    static contextType = LanguageContext;
    context!: ContextType<typeof LanguageContext>;

    public constructor(props: CarouselProps | Readonly<CarouselProps>) {
        super(props);

        this.state = {
            index: 0
        };

        this.onClickBefore = this.onClickBefore.bind(this);
        this.onClickNext = this.onClickNext.bind(this);
        this.onClickIndex = this.onClickIndex.bind(this);
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    private onClickBefore(event: MouseEvent<HTMLButtonElement>) {

        this.setState((current) => {

            let newIndex = current.index;
            if (current.index >= 1) {
                newIndex = current.index - 1;
            } else if (current.index === 0) {
                newIndex = this.props.children.length - 1;
            }

            return {
                index: newIndex
            };
        });
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    private onClickNext(event: MouseEvent<HTMLButtonElement>) {
        this.setState((current) => {

            let newIndex = current.index;
            if (current.index <= this.props.children.length - 2) {
                newIndex = current.index + 1;
            } else if (current.index === this.props.children.length - 1) {
                newIndex = 0;
            }

            return {
                index: newIndex
            };
        });
    }

    private onClickIndex(event: MouseEvent<HTMLButtonElement>) {

        const dataIndex = event.currentTarget.attributes.getNamedItem('data-index');

        this.setState((current) => {

            let newIndex = current.index;

            if (dataIndex !== null) {
                newIndex = Number(dataIndex.value);
            }
            return {
                index: newIndex
            };
        });
    }

    public render(): ReactNode {

        const indicatorClass = this.props.indicatorPosition === 'bottom' ? classes.indicator_bottom : classes.indicator_top;

        return (
            <div className={[classes.carousel, this.props.className].join(' ')}>

                <ul className={classes.list}>
                    {this.props.children[this.state.index]}
                </ul>

                <ul className={classes.controls}>
                    <li>
                        <button
                            title={strings[this.context.language]['button-previous']}
                            className={classes.control_previous}
                            onClick={this.onClickBefore}>
                            <BeforeIcon />
                        </button>
                    </li>
                    <li>
                        <button
                            title={strings[this.context.language]['button-next']}
                            className={classes.control_next}
                            onClick={this.onClickNext}>
                            <NextIcon />
                        </button>
                    </li>
                </ul>

                <ul className={[classes.indicator, indicatorClass].join(' ')}>

                    {this.props.children.map((item, index) => {

                        const classActive = this.state.index === index ? classes.indicator_item_active : ' ';

                        return (
                            <li key={index}>
                                <button
                                    title={`${strings[this.context.language]['item']} ${index + 1}`}
                                    className={[classes.indicator_item_button, classActive].join(' ')}
                                    data-index={index}
                                    onClick={this.onClickIndex} />
                            </li>
                        );
                    })}
                </ul>
            </div >

        );
    }

}

export interface CarouselItemProps {
    children: ReactNode | ReactNode[];
    className?: string;
}

export class CarouselItem extends Component<CarouselItemProps> {

    public constructor(props: CarouselItemProps | Readonly<CarouselItemProps>) {
        super(props);
    }

    public render(): ReactNode {
        return (
            <li className={this.props.className}>
                {this.props.children}
            </li>
        );
    }

}