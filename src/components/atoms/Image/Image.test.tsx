import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { Image } from './Image';
import logo from '../../../assets/images/logo.png';
import { LanguageProvider } from '../../../context/LanguageContext/LanguageContext';
import strings from './strings.json';

import { axe, toHaveNoViolations } from 'jest-axe';
expect.extend(toHaveNoViolations);


describe('Image component test', () => {

    const language = 'en-us';

    it('render', async () => {
        render(
            <LanguageProvider>
                <Image className={'class'} source={logo} alt={'logo'} />
            </LanguageProvider>
        );
        const image = screen.getByAltText('logo');
        expect(image).toBeInTheDocument();
    });

    it('accessibility', async () => {
        const { container } = render(
            <LanguageProvider>
                <Image className={'class'} source={logo} alt={'logo'} />
            </LanguageProvider>
        );
        const image = screen.getByAltText('logo');
        expect(image).toBeInTheDocument();

        const results = await axe(container);
        expect(results).toHaveNoViolations();
    });

    it('accessibility on error', async () => {
        const { container } = render(
            <LanguageProvider>
                <Image className={'class'} source={''} alt={'logo'} />
            </LanguageProvider>
        );
        const image = screen.getByAltText('logo');
        expect(image).toBeInTheDocument();
        fireEvent.error(image);
        expect(image).not.toBeInTheDocument();

        const missingIcon = screen.getByTitle(strings[language]['missing']);
        expect(missingIcon).toBeInTheDocument();

        const results = await axe(container);
        expect(results).toHaveNoViolations();
    });

});

describe('[Props] Image component test', () => {

    const language = 'en-us';

    it('source as string', () => {
        render(
            <LanguageProvider>
                <Image className={'class'} source={logo} alt={'logo'} />
            </LanguageProvider>
        );
        const image = screen.getByAltText('logo');
        expect(image).toBeInTheDocument();

        fireEvent.load(image);
        expect(image).toHaveAttribute('src', logo);
    });

    it('invalid source as string', () => {
        render(
            <LanguageProvider>
                <Image className={'class'} source={''} alt={'logo'} />
            </LanguageProvider>
        );
        const image = screen.getByAltText('logo');
        expect(image).toBeInTheDocument();
        fireEvent.error(image);

        expect(image).not.toBeInTheDocument();

        const missingIcon = screen.getByTitle(strings[language]['missing']);
        expect(missingIcon).toBeInTheDocument();
    });


    it('update empty source', () => {

        const { container } = render(
            <LanguageProvider>
                <Image className={'class'} source={' '} alt={'logo'} />
            </LanguageProvider>
        );
        const image = screen.getByAltText('logo');
        expect(image).toBeInTheDocument();
        fireEvent.error(image);

        expect(image).not.toBeInTheDocument();

        const missingIcon = screen.getByTitle(strings[language]['missing']);
        expect(missingIcon).toBeInTheDocument();

        render(
            <LanguageProvider>
                <Image className={'class'} source={logo} alt={'logo'} />
            </LanguageProvider>
            , { container });

        const i = screen.getByAltText('logo');
        expect(i).toBeInTheDocument();

        fireEvent.load(i);
        expect(i).toHaveAttribute('src', logo);
    });

    it('className', async () => {
        render(
            <LanguageProvider>
                <Image className={'class'} source={logo} alt={'logo'} />
            </LanguageProvider>
        );
        const image = screen.getByAltText('logo');
        expect(image).toBeInTheDocument();
        expect(image).toHaveClass('class');
    });
});

describe('[en-us] Image component test', () => {

    const language = 'en-us';

    beforeAll(() => {
        Object.defineProperty(navigator, 'language', {
            value: language,
            configurable: true
        });
    });

    it('invalid source as string', () => {
        render(
            <LanguageProvider>
                <Image className={'class'} source={''} alt={'logo'} />
            </LanguageProvider>
        );
        const image = screen.getByAltText('logo');
        expect(image).toBeInTheDocument();
        fireEvent.error(image);

        expect(image).not.toBeInTheDocument();

        const missingIcon = screen.getByTitle(strings[language]['missing']);
        expect(missingIcon).toBeInTheDocument();
    });

});

describe('[pt-br] Image component test', () => {

    const language = 'pt-br';

    beforeAll(() => {
        Object.defineProperty(navigator, 'language', {
            value: language,
            configurable: true
        });
    });

    it('invalid source as string', () => {
        render(
            <LanguageProvider>
                <Image className={'class'} source={''} alt={'logo'} />
            </LanguageProvider>
        );
        const image = screen.getByAltText('logo');
        expect(image).toBeInTheDocument();
        fireEvent.error(image);

        expect(image).not.toBeInTheDocument();

        const missingIcon = screen.getByTitle(strings[language]['missing']);
        expect(missingIcon).toBeInTheDocument();
    });

});