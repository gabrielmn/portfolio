import React from 'react';
import { ComponentMeta, Story } from '@storybook/react';
import logo from '../../../assets/images/logo.png';
import { Image } from './Image';

/** TODO
 * source control as file not working
 */

export default {
    title: 'Atoms/Image',
    component: Image,
    argTypes:{
        source:{
            control: {
                type: 'file',
                accept: '.png'
            }
        },
        className:{
            control: false
        }
    },
    parameters: {
        layout: 'centered'
    },
} as ComponentMeta<typeof Image>;

const template: Story = (args) => <Image alt={args.alt} source={args.source} {...args} />;

export const basic = template.bind({});
basic.args = {
    id: 'storybook-image',
    source: logo
};

export const missing = template.bind({});
missing.args = {
    id: 'storybook-image',
    source: 'wrong'
};
