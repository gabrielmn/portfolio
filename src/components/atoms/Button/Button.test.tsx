import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { Button } from './Button';

import { axe, toHaveNoViolations } from 'jest-axe';
expect.extend(toHaveNoViolations);

const onClick = jest.fn();

describe('Button component test', () => {

    it('render', async () => {
        render(
            <Button
                onClick={onClick}
                className={'class'}>
                Button
            </Button>);
        const elementLink = screen.getByText('Button');
        expect(elementLink).toBeInTheDocument();
    });

    it('accessibility', async () => {
        const { container } = render(
            <Button
                onClick={onClick}
                className={'class'}>
                Button
            </Button>);
        const elementLink = screen.getByText('Button');
        expect(elementLink).toBeInTheDocument();
        const results = await axe(container);
        expect(results).toHaveNoViolations();
    });
});

describe('[Props] Button component test', () => {

    it('children', () => {
        render(
            <Button
                onClick={onClick}
                className={'class'}>
                Button
            </Button>);
        const elementLink = screen.getByText('Button');
        expect(elementLink).toBeInTheDocument();
    });

    it('onClick', () => {
        render(
            <Button
                onClick={onClick}
                className={'class'}>
                Button
            </Button>);
        const elementLink = screen.getByText('Button');
        fireEvent.click(elementLink);
        expect(onClick).toHaveBeenCalledTimes(1);
    });

    it('className', () => {
        render(
            <Button
                onClick={onClick}
                className={'class'}>
                Button
            </Button>);
        const elementLink = screen.getByText('Button');
        expect(elementLink.className.includes('class')).toBe(true);
    });
});