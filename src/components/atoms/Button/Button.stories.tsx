import React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import { Button } from './Button';

export default {
    title: 'atoms/Button',
    component: Button,
    argTypes: {
        children: {
            control: 'text',
            defaultValue: 'Button'
        },
        onClick: {
            action: 'clicked'
        },
        className: {
            control: 'text'
        }
    },
    parameters: {
        layout: 'centered'
    }
} as ComponentMeta<typeof Button>;

const template: ComponentStory<typeof Button> = (args) => (

    <Button
        onClick={args.onClick}
        className={args.className}>
        {args.children}
    </Button>
);

export const basic = template.bind({});
