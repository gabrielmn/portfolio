import React from 'react';
import { render, screen } from '@testing-library/react';
import { Text } from './Text';

import { axe, toHaveNoViolations } from 'jest-axe';
expect.extend(toHaveNoViolations);

describe('Text component test', () => {

    it('render', async () => {
        render(<Text>text</Text>);
        const text = screen.getByText('text');
        expect(text).toBeInTheDocument();
    });

    it('accessibility', async () => {
        const { container } = render(<Text >text</Text>);
        const text = screen.getByText('text');
        expect(text).toBeInTheDocument();

        const results = await axe(container);
        expect(results).toHaveNoViolations();
    });

});

describe('[Props] Text component test', () => {

    it('text', () => {
        render(<Text >text</Text>);
        const text = screen.getByText('text');
        expect(text).toBeInTheDocument();
    });

    it('className', () => {
        render(<Text  className={'class'}>text</Text>);
        const text = screen.getByText('text');
        expect(text).toBeInTheDocument();
        expect(text).toHaveClass('class');
    });
});