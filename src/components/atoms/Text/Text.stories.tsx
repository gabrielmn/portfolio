import React from 'react';
import { ComponentMeta, Story } from '@storybook/react';
import { Text } from './Text';

export default {
    title: 'Atoms/Text',
    component: Text,
    argTypes: {
        text:{
            control: 'text',
            defaultValue:'Text'
        },
        className: {
            control: false
        }
    },
    parameters: {
        layout: 'centered'
    }
} as ComponentMeta<typeof Text>;

const template: Story = (args) => (<Text>{args.text}</Text>);

export const basic = template.bind({});
