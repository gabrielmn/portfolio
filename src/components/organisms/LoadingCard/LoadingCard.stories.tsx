import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import { LoadingCard } from './LoadingCard';

export default {
    title: 'Organisms/LoadingCard',
    component: LoadingCard,
    argTypes: {
        className: {
            control: false
        }
    },
    parameters: {
        layout: 'centered'
    },
} as ComponentMeta<typeof LoadingCard>;

const template: ComponentStory<typeof LoadingCard> = (args) => <LoadingCard {...args} />;

export const basic = template.bind({});

basic.args = {
    title: 'Loading message'
};
