import React from 'react';
import { render, screen } from '@testing-library/react';
import { LoadingCard } from './LoadingCard';

import { axe, toHaveNoViolations } from 'jest-axe';
expect.extend(toHaveNoViolations);

const title:string = 'Loading title';

describe('LoadingCard component test', () => {

    test('render', async () => {
        render(<LoadingCard title={title} loading={true} />);
        const titleNode = screen.getByText(title);
        expect(titleNode).toBeInTheDocument();
    });

    test('accessibility', async () => {
        const { container } = render(<LoadingCard title={title} loading={true} />
        );
        const titleNode = screen.getByText(title);
        expect(titleNode).toBeInTheDocument();
        
        // disable due to testing without a list container(ul)
        const results = await axe(container);
        expect(results).toHaveNoViolations();
    });
});

describe('[Props] LoadingCard component test', () => {

    test('title', () => {
        render(<LoadingCard title={title} loading={true} />);
        const titleNode = screen.getByText(title);
        expect(titleNode).toBeInTheDocument();
    });

    test('className', async () => {
        render(<LoadingCard className={'class'} title={title} loading={true} />);
        const parent = screen.getByText(title).parentNode;        
        expect(parent).toHaveClass('class');
    });
});