import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import { LanguageProvider } from '../../../context/LanguageContext/LanguageContext';

import { Header } from './Header';

import classes from './Header.module.css';
import strings from './strings.json';

import { axe, toHaveNoViolations } from 'jest-axe';
expect.extend(toHaveNoViolations);


describe('Header component test', () => {

    const language = 'en-us';

    test('render', () => {
        render(
            <LanguageProvider >
                <MemoryRouter initialEntries={['/']}>
                    <Header />
                </MemoryRouter>
            </LanguageProvider>
        );
        const brand = screen.getByText(strings[language]['brand']);
        expect(brand).toBeInTheDocument();
        const experience = screen.getByText(strings[language]['experience']);
        expect(experience).toBeInTheDocument();
        const projects = screen.getByText(strings[language]['projects']);
        expect(projects).toBeInTheDocument();
        const certificates = screen.getByText(strings[language]['certificates']);
        expect(certificates).toBeInTheDocument();
        const contact = screen.getByText(strings[language]['certificates']);
        expect(contact).toBeInTheDocument();
    });

    test('accessibility', async () => {
        const { container } = render(
            <LanguageProvider >
                <MemoryRouter initialEntries={['/']}>
                    <Header />
                </MemoryRouter>
            </LanguageProvider>
        );
        const brand = screen.getByText(strings[language]['brand']);
        expect(brand).toBeInTheDocument();
        const experience = screen.getByText(strings[language]['experience']);
        expect(experience).toBeInTheDocument();
        const projects = screen.getByText(strings[language]['projects']);
        expect(projects).toBeInTheDocument();
        const certificates = screen.getByText(strings[language]['certificates']);
        expect(certificates).toBeInTheDocument();
        const contact = screen.getByText(strings[language]['certificates']);
        expect(contact).toBeInTheDocument();

        const results = await axe(container);
        expect(results).toHaveNoViolations();
    });
});

describe('[Routes] Header component test', () => {

    const language = 'en-us';

    test('/', () => {
        render(
            <LanguageProvider >
                <MemoryRouter initialEntries={['/contact']}>
                    <Header />
                </MemoryRouter>
            </LanguageProvider>
        );
        const brand = screen.getByText(strings[language]['brand']);
        expect(brand).toBeInTheDocument();
        expect(brand).not.toHaveClass(classes.header_link_active);
        fireEvent.click(brand);
        expect(brand).toHaveClass(classes.header_link_active);
    });

    test('/experience', () => {
        render(
            <LanguageProvider >
                <MemoryRouter initialEntries={['/']}>
                    <Header />
                </MemoryRouter>
            </LanguageProvider>
        );
        const experience = screen.getByText(strings[language]['experience']);
        expect(experience).toBeInTheDocument();
        expect(experience).not.toHaveClass(classes.header_link_active);
        fireEvent.click(experience);
        expect(experience).toHaveClass(classes.header_link_active);
    });

    test('/projects', () => {
        render(
            <LanguageProvider >
                <MemoryRouter initialEntries={['/']}>
                    <Header />
                </MemoryRouter>
            </LanguageProvider>
        );
        const projects = screen.getByText(strings[language]['projects']);
        expect(projects).toBeInTheDocument();
        expect(projects).not.toHaveClass(classes.header_link_active);
        fireEvent.click(projects);
        expect(projects).toHaveClass(classes.header_link_active);
    });

    test('/certificates', () => {
        render(
            <LanguageProvider >
                <MemoryRouter initialEntries={['/']}>
                    <Header />
                </MemoryRouter>
            </LanguageProvider>
        );
        const certificates = screen.getByText(strings[language]['certificates']);
        expect(certificates).toBeInTheDocument();
        expect(certificates).not.toHaveClass(classes.header_link_active);
        fireEvent.click(certificates);
        expect(certificates).toHaveClass(classes.header_link_active);
    });

    test('/contact', () => {
        render(
            <LanguageProvider >
                <MemoryRouter initialEntries={['/']}>
                    <Header />
                </MemoryRouter>
            </LanguageProvider>
        );
        const contact = screen.getByText(strings[language]['certificates']);
        expect(contact).toBeInTheDocument();
        expect(contact).not.toHaveClass(classes.header_link_active);
        fireEvent.click(contact);
        expect(contact).toHaveClass(classes.header_link_active);
    });
});

describe('[en-us] Header component test', () => {

    const language = 'en-us';

    beforeAll(() => {
        Object.defineProperty(navigator, 'language', {
            value: language,
            configurable: true
        });
    });

    test('brand', () => {

        render(
            <LanguageProvider >
                <MemoryRouter initialEntries={['/']}>
                    <Header />
                </MemoryRouter>
            </LanguageProvider>
        );
        const brand = screen.getByText(strings[language]['brand']);
        expect(brand).toBeInTheDocument();
    });


    test('experience', () => {

        render(
            <LanguageProvider >
                <MemoryRouter initialEntries={['/']}>
                    <Header />
                </MemoryRouter>
            </LanguageProvider>
        );
        const experience = screen.getByText(strings[language]['experience']);
        expect(experience).toBeInTheDocument();
    });

    test('projects', () => {

        render(
            <LanguageProvider >
                <MemoryRouter initialEntries={['/']}>
                    <Header />
                </MemoryRouter>
            </LanguageProvider>
        );
        const projects = screen.getByText(strings[language]['projects']);
        expect(projects).toBeInTheDocument();
    });

    test('certificates', () => {

        render(
            <LanguageProvider >
                <MemoryRouter initialEntries={['/']}>
                    <Header />
                </MemoryRouter>
            </LanguageProvider>
        );
        const certificates = screen.getByText(strings[language]['certificates']);
        expect(certificates).toBeInTheDocument();
    });

    test('contact', () => {

        render(
            <LanguageProvider >
                <MemoryRouter initialEntries={['/']}>
                    <Header />
                </MemoryRouter>
            </LanguageProvider>
        );
        const contact = screen.getByText(strings[language]['certificates']);
        expect(contact).toBeInTheDocument();
    });

});

describe('[pt-br] Header component test', () => {

    const language = 'pt-br';

    beforeAll(() => {
        Object.defineProperty(navigator, 'language', {
            value: language,
            configurable: true
        });
    });

    test('brand', () => {

        render(
            <LanguageProvider >
                <MemoryRouter initialEntries={['/']}>
                    <Header />
                </MemoryRouter>
            </LanguageProvider>
        );
        const brand = screen.getByText(strings[language]['brand']);
        expect(brand).toBeInTheDocument();
    });


    test('experience', () => {

        render(
            <LanguageProvider >
                <MemoryRouter initialEntries={['/']}>
                    <Header />
                </MemoryRouter>
            </LanguageProvider>
        );
        const experience = screen.getByText(strings[language]['experience']);
        expect(experience).toBeInTheDocument();
    });

    test('projects', () => {

        render(
            <LanguageProvider >
                <MemoryRouter initialEntries={['/']}>
                    <Header />
                </MemoryRouter>
            </LanguageProvider>
        );
        const projects = screen.getByText(strings[language]['projects']);
        expect(projects).toBeInTheDocument();
    });

    test('certificates', () => {

        render(
            <LanguageProvider >
                <MemoryRouter initialEntries={['/']}>
                    <Header />
                </MemoryRouter>
            </LanguageProvider>
        );
        const certificates = screen.getByText(strings[language]['certificates']);
        expect(certificates).toBeInTheDocument();
    });

    test('contact', () => {

        render(
            <LanguageProvider >
                <MemoryRouter initialEntries={['/']}>
                    <Header />
                </MemoryRouter>
            </LanguageProvider>
        );
        const contact = screen.getByText(strings[language]['certificates']);
        expect(contact).toBeInTheDocument();
    });

});