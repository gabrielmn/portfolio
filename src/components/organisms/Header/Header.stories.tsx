import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { MemoryRouter } from 'react-router-dom';

import { LanguageProvider } from '../../../context/LanguageContext/LanguageContext';
import { Header } from './Header';

export default {
    title: 'organisms/Header',
    component: Header,
    parameters: {
        layout: 'fullscreen'
    },
    decorators: [
        (Story) => (
            <LanguageProvider>
                <MemoryRouter initialEntries={['/']}>
                    <Story />
                </MemoryRouter>
            </LanguageProvider>
        )
    ]
} as ComponentMeta<typeof Header>;

const Template: ComponentStory<typeof Header> = (args) => <Header {...args} />;

export const Primary = Template.bind({});
