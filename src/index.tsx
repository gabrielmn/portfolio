import React from 'react';
import ReactDOM from 'react-dom/client';
import { App } from './App';
// import contexts
import { LanguageProvider } from './context/LanguageContext/LanguageContext';
// 
import { BrowserRouter } from 'react-router-dom';
// 
import reportWebVitals from './reportWebVitals';
// import shared css
import './index.css';

ReactDOM.createRoot(
    document.getElementById('root') as HTMLElement
).render(
    <React.StrictMode>
        <LanguageProvider>
            <BrowserRouter>
                <App />
            </BrowserRouter>
        </LanguageProvider>
    </React.StrictMode>
);


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
