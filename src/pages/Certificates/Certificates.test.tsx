import React from 'react';
import { render, screen } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { LanguageProvider } from '../../context/LanguageContext/LanguageContext';
import { Certificates } from './Certificates';
import strings from './strings.json'

import { axe, toHaveNoViolations } from 'jest-axe';
expect.extend(toHaveNoViolations);

describe('Certificates component test', () => {
    
    const language = 'en-us';
    const data = [
        {
            "id": "1",
            "name": "Introduction to Networking",
            "conclusionDate": "2023-10-01",
            "link": "https://www.udemy.com/certificate/UC-cd6445d9-c4bc-44f6-b687-08add528a4e8/",
            "topics": [
                { "id": 1, "topic": "A basic understanding of networking" },
                { "id": 2, "topic": "Understand IP address assignment using DHCP (Dynamic Host Configuration Protocol)" },
                { "id": 3, "topic": "Understand IPv4 addressing and subnetting" },
                { "id": 4, "topic": "Understand name resolution using DNS (Domain Naming Service)" },
                { "id": 5, "topic": "Understand IPv6 addressing" }
            ]
        }
    ]

    beforeEach(() => {
        // Mock the global fetch function
        global.fetch = jest.fn(() =>
            Promise.resolve({
                status: 200,
                json: () => Promise.resolve(data),
            })
        ) as jest.Mock;
    })

    // check if all the components in the element has rendered
    test('render', async () => {

        await act(async () => {
            render(
                <LanguageProvider >
                    <Certificates />
                </LanguageProvider>
            );
        })

        expect(document.title).toBe(strings[language]['title']);

        data.forEach((item) => {

            const title = screen.getByText(item.name);
            expect(title).toBeInTheDocument();

            const date = title.nextSibling?.textContent?.trim();
            expect(date).toBe(item.conclusionDate);

            item.topics.forEach((topic) => {
                const description = screen.getByText(topic.topic);
                expect(description).toBeInTheDocument();
            });

            const link = screen.getByText(item.link);
            expect(link).toBeInTheDocument();

        });
    });

    test('accessibility', async () => {

        let container;

        await act(async () => {
            const rendered = render(
                <LanguageProvider >
                    <Certificates />
                </LanguageProvider>
            );
            container = rendered.container;
        })

        expect(document.title).toBe(strings[language]['title']);

        data.forEach((item) => {

            const title = screen.getByText(item.name);
            expect(title).toBeInTheDocument();

            const date = title.nextSibling?.textContent?.trim();
            expect(date).toBe(item.conclusionDate);

            item.topics.forEach((topic) => {
                const description = screen.getByText(topic.topic);
                expect(description).toBeInTheDocument();
            });

            const link = screen.getByText(item.link);
            expect(link).toBeInTheDocument();

        });

        expect(container).not.toBe(undefined);
        if (container) {
            const results = await axe(container);
            expect(results).toHaveNoViolations();
        }

    });
});