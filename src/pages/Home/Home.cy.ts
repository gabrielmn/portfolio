export { };

describe('Home page', () => {

    it('accessibility', () => {
        cy.visit('http://localhost:3000');
        cy.injectAxe();
        cy.checkA11y();
    });

    // it('check cv download', () => {

    //     cy.task('rm', `${Cypress.config('downloadsFolder')}/*.pdf`);

    //     cy.visit('http://localhost:3000');

    //     cy.get('button').click();
    //     cy.readFile(`${Cypress.config('downloadsFolder')}/*.pdf`);
    // });

});