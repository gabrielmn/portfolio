import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { LanguageProvider } from '../../context/LanguageContext/LanguageContext';
import { Home } from './Home';

import { axe, toHaveNoViolations } from 'jest-axe';
expect.extend(toHaveNoViolations);

import strings from './strings.json';

describe('Home component test', () => {

    const language = 'en-us';

    it('render', async () => {
        render(
            <LanguageProvider>
                <Home />
            </LanguageProvider>
        );
        const username = screen.getByText(strings[language]['username']);
        expect(username).toBeInTheDocument();
        const role = screen.getByText(strings[language]['role']);
        expect(role).toBeInTheDocument();
        const button = screen.getByText(strings[language]['button']);
        expect(button).toBeInTheDocument();
    });

    it('accessibility', async () => {
        const { container } = render(
            <LanguageProvider>
                <Home />
            </LanguageProvider>
        );
        const username = screen.getByText(strings['en-us']['username']);
        expect(username).toBeInTheDocument();
        const role = screen.getByText(strings['en-us']['role']);
        expect(role).toBeInTheDocument();
        const button = screen.getByText(strings['en-us']['button']);
        expect(button).toBeInTheDocument();
        const results = await axe(container);
        expect(results).toHaveNoViolations();
    });

    it('download', async () => {
        
        const onClick = jest.fn();
        const link = document.createElement('a');
        link.onclick = onClick;
        
        render(
            <LanguageProvider>
                <Home />
            </LanguageProvider>
        );

        const username = screen.getByText(strings[language]['username']);
        expect(username).toBeInTheDocument();
        const role = screen.getByText(strings[language]['role']);
        expect(role).toBeInTheDocument();
        const button = screen.getByText(strings[language]['button']);
        expect(button).toBeInTheDocument();
        
        const spy = jest.spyOn(document, 'createElement').mockImplementation(() => link);
        
        fireEvent.click(button);
        expect(onClick).toHaveBeenCalledTimes(1);
        spy.mockRestore();
        
    });
});

describe('[en-us] Home component test', () => {

    const language = 'en-us';

    beforeAll(() => {
        Object.defineProperty(navigator, 'language', {
            value: language,
            configurable: true
        });
    });

    test('username', () => {
        render(
            <LanguageProvider >
                <Home />
            </LanguageProvider>
        );
        const username = screen.getByText(strings[language]['username']);
        expect(username).toBeInTheDocument();
    });

    test('role', () => {
        render(
            <LanguageProvider >
                <Home />
            </LanguageProvider>
        );
        const role = screen.getByText(strings[language]['role']);
        expect(role).toBeInTheDocument();
    });

    test('button', () => {
        render(
            <LanguageProvider >
                <Home />
            </LanguageProvider>
        );
        const button = screen.getByText(strings[language]['button']);
        expect(button).toBeInTheDocument();
    });

});

describe('[pt-br] Header component test', () => {

    const language = 'pt-br';

    beforeAll(() => {
        Object.defineProperty(navigator, 'language', {
            value: language,
            configurable: true
        });
    });

    test('username', () => {
        render(
            <LanguageProvider >
                <Home />
            </LanguageProvider>
        );
        const username = screen.getByText(strings[language]['username']);
        expect(username).toBeInTheDocument();
    });

    test('role', () => {
        render(
            <LanguageProvider >
                <Home />
            </LanguageProvider>
        );
        const role = screen.getByText(strings[language]['role']);
        expect(role).toBeInTheDocument();
    });

    test('button', () => {
        render(
            <LanguageProvider >
                <Home />
            </LanguageProvider>
        );
        const button = screen.getByText(strings[language]['button']);
        expect(button).toBeInTheDocument();
    });

});