import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { LanguageProvider } from '../../context/LanguageContext/LanguageContext';
import { Home } from './Home';

export default {
    title: 'pages/Home',
    component: Home,
    parameters: {
        layout: 'fullscreen'
    },
    decorators:[
        (Story)=>
            <LanguageProvider>
                <Story/>
            </LanguageProvider>
    ]
} as ComponentMeta<typeof Home>;

const Template: ComponentStory<typeof Home> = (args) => <Home />;

export const Primary = Template.bind({});
