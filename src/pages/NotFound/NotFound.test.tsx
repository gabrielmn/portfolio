import React from 'react';
import { render, screen } from '@testing-library/react';
import { LanguageProvider } from '../../context/LanguageContext/LanguageContext';
import { NotFound } from './NotFound';

import { axe, toHaveNoViolations } from 'jest-axe';
expect.extend(toHaveNoViolations);

import strings from './strings.json';

describe('NotFound component test', () => {

    const language = 'en-us';

    it('render', async () => {

        render(
            <LanguageProvider>
                <NotFound />
            </LanguageProvider>
        );

        expect(document.title).toBe(strings[language]['title']);

        const title = screen.getByText(strings[language]['page-not-found']);
        expect(title).toBeInTheDocument();

        const instructions = screen.getByText(strings[language]['instructions']);
        expect(instructions).toBeInTheDocument();
    });

    it('accessibility', async () => {

        const { container } = render(
            <LanguageProvider>
                <NotFound />
            </LanguageProvider>
        );

        const title = screen.getByText(strings[language]['page-not-found']);
        expect(title).toBeInTheDocument();

        const instructions = screen.getByText(strings[language]['instructions']);
        expect(instructions).toBeInTheDocument();

        const results = await axe(container);
        expect(results).toHaveNoViolations();
    });

});