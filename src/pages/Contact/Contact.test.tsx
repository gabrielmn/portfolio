import React from 'react';
import { render, screen, getDefaultNormalizer } from '@testing-library/react';
import { LanguageProvider } from '../../context/LanguageContext/LanguageContext';
import { Contact } from './Contact';

import { axe, toHaveNoViolations } from 'jest-axe';
expect.extend(toHaveNoViolations);

import strings from './strings.json';

describe('Contact component test', () => {

    const language = 'en-us';

    // check if all the components in the element has rendered
    test('render', async () => {
        render(
            <LanguageProvider >
                <Contact />
            </LanguageProvider>
        );

        expect(document.title).toBe(strings[language]['title']);

        const instructionsElement = screen.getByText(strings[language]['contact-instructions'], { normalizer: getDefaultNormalizer({ trim: false }) });
        expect(instructionsElement).toBeInTheDocument();
        const emailElement = screen.getByText('gabriel.m.nori@hotmail.com');
        expect(emailElement).toBeInTheDocument();
    });

    test('accessibility', async () => {
        const { container } = render(
            <LanguageProvider >
                <Contact />
            </LanguageProvider>
        );

        const instructionsElement = screen.getByText(strings[language]['contact-instructions'], { normalizer: getDefaultNormalizer({ trim: false }) });
        expect(instructionsElement).toBeInTheDocument();
        const emailElement = screen.getByText('gabriel.m.nori@hotmail.com');
        expect(emailElement).toBeInTheDocument();

        const results = await axe(container);
        expect(results).toHaveNoViolations();
    });
});