import React from 'react';
import { render, screen } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { LanguageProvider } from '../../context/LanguageContext/LanguageContext';
import { Experiences } from './Experiences';
import strings from './strings.json';

import { axe, toHaveNoViolations } from 'jest-axe';
expect.extend(toHaveNoViolations);

describe('Experiences component test', () => {

    const language = 'en-us';
    const data = [
        {
            "id": "1",
            "type": "Internship",
            "company": "GLTI",
            "position": "System administrator",
            "startDate": "2023-05-01",
            "endDate": "2024-03-05",
            "image": "https://gportfolio.blob.core.windows.net/past-experiences/glti.png",
            "responsibilities": [
                { "id": 1, "responsibility": "Helpdesk" },
                { "id": 2, "responsibility": "Windows Server" },
                { "id": 3, "responsibility": "Tools development" }
            ]
        }
    ]

    beforeEach(() => {
        // Mock the global fetch function
        global.fetch = jest.fn(() =>
            Promise.resolve({
                status: 200,
                json: () => Promise.resolve(data),
            })
        ) as jest.Mock;
    })

    test('render', async () => {
        
        await act(async () => {
            render(
            <LanguageProvider>
                <Experiences/>
            </LanguageProvider>
            );    
        })

        expect(document.title).toBe(strings[language]['title']);

        data.forEach((experience) => {

            const company = screen.getByText(experience.company, { exact: false });
            expect(company).toBeInTheDocument();
            const position = screen.getByText(experience.position, { exact: false });
            expect(position).toBeInTheDocument();
            const startDate = screen.getByText(experience.startDate, { exact: false });
            expect(startDate).toBeInTheDocument();
            const endDate = startDate.nextSibling?.textContent?.split(":")[1].trim();
            expect(endDate).toBe(experience.endDate);

            experience.responsibilities.forEach((responsibility) => {
                const responsibilityText = screen.getByText(responsibility.responsibility);
                expect(responsibilityText).toBeInTheDocument();
            });
        });
    });

    test('accessibility', async () => {
        
        let container;

        await act(async () => {
            const rendered = render(
                <LanguageProvider >
                    <Experiences />
                </LanguageProvider>
            );
            container = rendered.container;
        })

        expect(document.title).toBe(strings[language]['title']);

        data.forEach((experience) => {

            const company = screen.getByText(experience.company, { exact: false });
            expect(company).toBeInTheDocument();
            const position = screen.getByText(experience.position, { exact: false });
            expect(position).toBeInTheDocument();
            const startDate = screen.getByText(experience.startDate, { exact: false });
            expect(startDate).toBeInTheDocument();
            const endDate = startDate.nextSibling?.textContent?.split(":")[1].trim();
            expect(endDate).toBe(experience.endDate);

            experience.responsibilities.forEach((responsibility) => {
                const responsibilityText = screen.getByText(responsibility.responsibility);
                expect(responsibilityText).toBeInTheDocument();
            });
        });

        expect(container).not.toBe(undefined);
        if (container) {
            const results = await axe(container);
            expect(results).toHaveNoViolations();
        }
    });
});