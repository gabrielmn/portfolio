import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import { Experiences } from './Experiences';

export default {
    title: 'pages/Experiences',
    component: Experiences,
    parameters: {
        layout: 'fullscreen'
    }
} as ComponentMeta<typeof Experiences>;

const Template: ComponentStory<typeof Experiences> = (args) => <Experiences/>;

export const Primary = Template.bind({});
