export { };

describe('Home page', () => {

    beforeEach(() => {
        cy.visit('http://localhost:3000/experience');
    });

    it('accessibility', () => {

        cy.injectAxe();
        
        cy.checkA11y( undefined, {
            rules: { 
                'page-has-heading-one': { enabled: false }
            }
        });
    });

});