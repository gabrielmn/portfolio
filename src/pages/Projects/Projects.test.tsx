import React from 'react';
import { render, screen } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { LanguageProvider } from '../../context/LanguageContext/LanguageContext';
import { Projects } from './Projects';
import strings from './strings.json';

import { axe, toHaveNoViolations } from 'jest-axe';
expect.extend(toHaveNoViolations);

describe('Projects component test', () => {

    const language = 'en-us';
    const data = [
        {
            "id": "1",
            "name": "DDA Script",
            "description": "This project introduces a Python script tailored for Dungeon Defenders Awakened, designed to enable automatic tower buffing and accelerated resource collection while AFK. With official approval from the game developers, the script aligns with fair play guidelines. Users can customize buff intervals, allowing for strategic adjustments to defense mechanisms. Suitable for individuals with foundational Python knowledge, this script streamlines the enhancement of in-game defenses and resource gathering. Automating hero skills to fortify tower defenses significantly boosts the efficiency of acquiring loot, gold, and XP, proving to be an essential tool for committed players.",
            "image": "https://gitlab.com/uploads/-/system/project/avatar/45936726/dda.jpg",
            "repository": "https://gitlab.com/gabrielmn/dda-script", "topics": [
                { "id": 1, "topic": "python" }
            ]
        }
    ]

    beforeEach(() => {
        // Mock the global fetch function
        global.fetch = jest.fn(() =>
            Promise.resolve({
                status: 200,
                json: () => Promise.resolve(data),
            })
        ) as jest.Mock;
    })

    test('render', async () => {

        await act(async () => {
            render(
                <LanguageProvider>
                    <Projects />
                </LanguageProvider>
            );
        })

        expect(document.title).toBe(strings[language]['title']);

        data.forEach((project) => {

            const name = screen.getByText(project.name);
            expect(name).toBeInTheDocument();
            
            project.topics.forEach((topic) => {
                const topicText = screen.getByText(topic.topic);
                expect(topicText).toBeInTheDocument();
            });
        });
    });

    test('accessibility', async () => {

        let container;

        await act(async () => {
            const rendered = render(
                <LanguageProvider >
                    <Projects />
                </LanguageProvider>
            );
            container = rendered.container;
        })

        expect(document.title).toBe(strings[language]['title']);

        
        expect(document.title).toBe(strings[language]['title']);

        data.forEach((project) => {

            const name = screen.getByText(project.name);
            expect(name).toBeInTheDocument();
            
            project.topics.forEach((topic) => {
                const topicText = screen.getByText(topic.topic);
                expect(topicText).toBeInTheDocument();
            });
        });

        expect(container).not.toBe(undefined);
        if (container) {
            const results = await axe(container);
            expect(results).toHaveNoViolations();
        }
    });
});