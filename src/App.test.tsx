import React from 'react';
import { render, screen, getDefaultNormalizer } from '@testing-library/react';
import { LanguageProvider } from './context/LanguageContext/LanguageContext';
import { MemoryRouter } from 'react-router-dom';
import { App } from './App';

import { axe, toHaveNoViolations } from 'jest-axe';
expect.extend(toHaveNoViolations);

import stringsHome from './pages/Home/strings.json';
import stringsExperiences from './pages/Experiences/strings.json';
import stringsProjects from './pages/Projects/strings.json';
import stringsCertificates from './pages/Certificates/strings.json';
import stringsContact from './pages/Contact/strings.json';
import stringsNotFound from './pages/NotFound/strings.json';

describe('[Routes] App component test', () => {

    const language = 'en-us';

    test('Home', async () => {

        render(
            <LanguageProvider>
                <MemoryRouter initialEntries={['/']}>
                    <App />
                </MemoryRouter>
            </LanguageProvider>
        );
        
        expect(document.title).toBe(stringsHome[language]['title']);

    });

    test('Experiences', async () => {

        render(
            <LanguageProvider>
                <MemoryRouter initialEntries={['/experience']}>
                    <App />
                </MemoryRouter>
            </LanguageProvider>
        );
        expect(document.title).toBe(stringsExperiences[language]['title']);
    });

    test('Projects', async () => {

        render(
            <LanguageProvider>
                <MemoryRouter initialEntries={['/projects']}>
                    <App />
                </MemoryRouter>
            </LanguageProvider>
        );
        expect(document.title).toBe(stringsProjects[language]['title']);
        
    });

    test('Certificates', async () => {
        
        render(
            <LanguageProvider >
                <MemoryRouter initialEntries={['/certificates']}>
                    <App />
                </MemoryRouter>
            </LanguageProvider>
        );
        expect(document.title).toBe(stringsCertificates[language]['title']);
        
    });

    test('Contact', async () => {

        render(
            <LanguageProvider>
                <MemoryRouter initialEntries={['/contact']}>
                    <App />
                </MemoryRouter>
            </LanguageProvider>
        );
        expect(document.title).toBe(stringsContact[language]['title']);
        
    });
    
    test('NotFound', async () => {

        render(
            <LanguageProvider>
                <MemoryRouter initialEntries={['/not-found']}>
                    <App />
                </MemoryRouter>
            </LanguageProvider>
        );
        expect(document.title).toBe(stringsNotFound[language]['title']);
        
    });

});