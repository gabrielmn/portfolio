**As a** _tech recruiter_

**I want to** _open the website on my phone_

**so that I can** _browse the site on my phone_

**Acceptance criteria**
- Support resolution 360×640
- Support resolution 414×896



/label ~"User Story"
