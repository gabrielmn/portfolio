**As a** _tech recruiter_

**I want to** _open the website on notebook

**so that I can** _browse the site on notebook_

**Acceptance criteria**
- Support resolution 1280×720
- Support resolution 1920×1080



/label ~"User Story"
