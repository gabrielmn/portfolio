// Router
import { MemoryRouter } from 'react-router-dom';
// context
import { LanguageProvider } from '../src/context/LanguageContext/LanguageContext';
// css with all shared proprieties
import '../src/index.css';


export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}


export const decorators = [
  (Story) => (
    <LanguageProvider>
      <MemoryRouter>
        <Story />
      </MemoryRouter>
    </LanguageProvider>
  ),
];
